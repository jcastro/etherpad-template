Etherpad template for docker swarm
==================================

This dockerfile creates a swarm stack with etherpad using nginx as proxy
and oauth as authentication mechanism

Usage
-----

Clone the repository locally and generate the APIKEY and SESSIONKEY files

```
openssl rand -hex 32 > ./etherpad_apikey.txt
openssl rand -hex 32 > ./etherpad_sessionkey.txt
```

Request a certificate for the alias required and place the cert and key
as the files nginx_cert.pem and nginx_key.pem


Adapt the etherpad_settings_sample.json file by replacing CHANGEME to your
setup details, save the adapted version as etherpad_settings.json file

Adapt nginx_proxy_sample.conf file as well with your specific details

Once done, just deploy it in a docker swarm cluster
```
docker stack deploy --compose-file=etherpad.yml etherpad
```

Now connect to your alias from your browser.
