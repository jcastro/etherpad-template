server {
    listen       443;
    server_name  CHANGEME;

    client_max_body_size 200m;
    proxy_buffers 16 64k;
    proxy_buffer_size 128k;
    proxy_connect_timeout       600;
    proxy_send_timeout          600;
    proxy_read_timeout          600;
    send_timeout                600;

    ssl                  on;
    ssl_certificate      /run/secrets/cert.pem;
    ssl_certificate_key  /run/secrets/key.pem;
    ssl_session_timeout  5m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS";

    location / {
        resolver 127.0.0.11 ipv6=off;
        proxy_pass             http://backend.internal:9001/;
        proxy_set_header       Host $host;
        proxy_pass_header Server;
        # be carefull, this line doesn't override any proxy_buffering on set in a conf.d/file.conf
        proxy_buffering off;
        proxy_set_header X-Real-IP $remote_addr;  # http://wiki.nginx.org/HttpProxyModule
        proxy_set_header X-Forwarded-For $remote_addr; # EP logs to show the actual remote IP
        proxy_set_header X-Forwarded-Proto $scheme; # for EP to set secure cookie flag when https is used
        proxy_set_header Host $host;  # pass the host header
        proxy_http_version 1.1;  # recommended with keepalive connections
        # WebSocket proxying - from http://nginx.org/en/docs/http/websocket.html
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }
    location /socket.io {
        rewrite /socket.io/(.*) /socket.io/$1 break;
        resolver 127.0.0.11 ipv6=off;
        proxy_pass             http://backend.internal:9001/;
        proxy_set_header Host $host;
        proxy_buffering off;
    }
    location /static {
        rewrite /static/(.*) /static/$1 break;
        resolver 127.0.0.11 ipv6=off;
        proxy_pass             http://backend.internal:9001/;
        proxy_set_header Host $host;
        proxy_buffering off;
    }
}

# we're in the http context here
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
    listen      80;
    server_name CHANGEME;
    rewrite     ^(.*)   https://$server_name$1 permanent;
}
